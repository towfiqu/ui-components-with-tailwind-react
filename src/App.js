import React from 'react';
import './css/tailwind.css';
import Navbar from './components/Navbar';

const App = () => {
  return (
    <div>
      <Navbar />
    </div>
  );
};

export default App;
