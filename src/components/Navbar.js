import React, { useState } from 'react';
import Dropdown from './Dropdown';
import profile_pic from '../assets/26.jpg';

const Navbar = () => {
  const [showNav, setShowNav] = useState(false);
  const onToggle = () => {
    setShowNav(!showNav);
  };
  let show_nav_items = 'transform -translate-x-64 fixed';
  let close_icons = 'hidden';
  let menu_icons = 'block';
  if (showNav) {
    show_nav_items = 'transition duration-500 transform translate-x-0';
    menu_icons = 'hidden';
    close_icons = 'block';
  }
  return (
    <div className='bg-gray-300'>
      <header className='md:flex md:justify-between md:w-3/4 w-4/5 m-auto'>
        <div className='flex items-center justify-between py-4 z-10'>
          <div>
            <a className='font-semibold text-2xl' href='#!'>
              Logo
            </a>
          </div>
          <div className='md:hidden' onClick={onToggle}>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              height='24'
              viewBox='0 0 24 24'
              width='24'
            >
              <path
                className={`${menu_icons}`}
                d='M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z'
              />
              <path
                className={close_icons}
                d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z'
              />
            </svg>
          </div>
        </div>
        {/* Mobile nav */}
        <div className={`${show_nav_items} py-4 font-semibold md:hidden`}>
          <a
            className='block rounded py-2 hover:text-gray-600 md:ml-4'
            href='#!'
          >
            Home
          </a>
          <a
            className='block font-semibold rounded py-2 hover:text-gray-600 md:ml-4'
            href='#!'
          >
            About
          </a>
          <a
            className='block font-semibold rounded py-2 hover:text-gray-600 md:ml-4'
            href='#!'
          >
            Contact
          </a>
          <div className='border-t border-gray-400 mt-2'></div>
          {/* Dropdown section for mobile nav */}
          <div className='flex items-center py-1 mt-4'>
            <img
              className='rounded-full w-8 h-8 border-2 border-gray-600'
              src={profile_pic}
              alt=''
            />
            <span className='font-bold ml-3'>John Doe</span>
          </div>

          <div>
            <a
              className='block font-normal py-1  hover:text-gray-700'
              href='#!'
            >
              Account settings
            </a>
            <a
              className='block font-normal py-1  hover:text-gray-700'
              href='#!'
            >
              Support
            </a>
            <a
              className='block font-normal py-1  hover:text-gray-700'
              href='#!'
            >
              Sign out
            </a>
          </div>
        </div>
        {/* Medium to large screen nav */}
        <div className='py-4 font-semibold hidden md:flex md:items-center'>
          <a
            className='block rounded py-2 hover:text-gray-600 md:ml-4'
            href='#!'
          >
            Home
          </a>
          <a
            className='block rounded py-2 hover:text-gray-600 md:ml-4'
            href='#!'
          >
            About
          </a>
          <a
            className='block rounded py-2 hover:text-gray-600 md:ml-4'
            href='#!'
          >
            Contact
          </a>
          <Dropdown />
        </div>
      </header>
    </div>
  );
};

export default Navbar;
