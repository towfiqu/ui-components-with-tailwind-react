import React, { useState } from 'react';
import profile_pic from '../assets/26.jpg';

const Dropdown = () => {
  const [open, setOpen] = useState(false);

  const toggleDropdown = () => {
    setOpen(!open);
  };
  let show_dropdown = 'hidden';
  if (open) {
    show_dropdown = 'block';
  }

  const handleClick = e => {
    if (e.key === 'Esc' || e.key === 'Escape') {
      setOpen(false);
    }
  };

  if (open) {
    document.addEventListener('keydown', handleClick);
  }

  return (
    <div className='relative ml-4'>
      <div className='relative z-10' onClick={toggleDropdown}>
        <img
          className='rounded-full w-8 h-8 border-2 border-gray-600'
          src={profile_pic}
          alt=''
        />
      </div>
      {open && (
        <div
          onClick={toggleDropdown}
          className='fixed top-0 right-0 bottom-0 left-0 bg-black opacity-50'
        ></div>
      )}

      <div
        className={`${show_dropdown} absolute right-0 w-48 mt-2 bg-gray-100 py-2 rounded-lg shadow`}
      >
        <a
          className='block px-4 py-2 hover:bg-indigo-500 hover:text-white'
          href='#!'
        >
          Account settings
        </a>
        <a
          className='block px-4 py-2 hover:bg-indigo-500 hover:text-white'
          href='#!'
        >
          Support
        </a>
        <a
          className='block px-4 py-2 hover:bg-indigo-500 hover:text-white'
          href='#!'
        >
          Sign out
        </a>
      </div>
    </div>
  );
};

export default Dropdown;
